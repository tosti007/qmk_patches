/* Copyright 2023 @ Brian Janssen <git at brianjanssen dot nl>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

/* RGB Matrix Configuration */
#ifdef RGB_MATRIX_ENABLE
#    define RGB_DISABLE_WHEN_USB_SUSPENDED
#    define RGB_INDICATOR_LIGHT_FLAGS LED_FLAG_MODIFIER
#    define RGB_INDICATOR_LIGHT_F_ROW
#    define RGB_DEFAULT_MODE RGB_MATRIX_SOLID_COLOR
#    define RGB_DEFAULT_COLOR HSV_GOLD
#    define RGB_CAPS_COLOR HSV_BLUE
#    define RGB_WORD_CAPS_COLOR HSV_CYAN
#endif

