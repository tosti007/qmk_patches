/* Copyright 2022 @ Teimor Epstein
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include QMK_KEYBOARD_H
#include "rgb_matrix_user.h"

void rgb_matrix_init_user(void) {
}

void rgb_matrix_post_init_user(void) {
    rgb_matrix_mode_noeeprom(RGB_DEFAULT_MODE);
    rgb_matrix_sethsv_noeeprom(RGB_DEFAULT_COLOR);
}

RGB rgb_from_hsv_with_val(uint8_t hue, uint8_t sat, uint8_t val) {
    HSV hsv = {hue, sat, val};
    val = rgb_matrix_get_val();
    if (hsv.v > val) {
        hsv.v = val;
    }
    return hsv_to_rgb(hsv);
}

void rgb_matrix_sethsv_or_default(uint8_t led_min, uint8_t led_max, uint8_t hue, uint8_t sat, uint8_t val) {
    RGB y = rgb_from_hsv_with_val(hue, sat, val);

    for (uint8_t i = led_min; i < led_max; i++) {
        #ifdef RGB_INDICATOR_LIGHT_F_ROW
            if ((i / MATRIX_COLS) != 0) break;
        #endif
        if ((g_led_config.flags[i] & RGB_INDICATOR_LIGHT_FLAGS) > 0) {
            rgb_matrix_set_color(i, y.r, y.g, y.b);
        }
    }
}

bool rgb_matrix_indicators_advanced_user(uint8_t led_min, uint8_t led_max) {
    if (host_keyboard_led_state().caps_lock) {
        rgb_matrix_sethsv_or_default(led_min, led_max, RGB_CAPS_COLOR);
        return false;
    }
    if (is_caps_word_on()) {
        rgb_matrix_sethsv_or_default(led_min, led_max, RGB_WORD_CAPS_COLOR);
        return false;
    }
    return false;
}
